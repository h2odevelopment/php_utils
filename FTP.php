<?php
/**
 * Ftp Class
 *
 * Each function returns a boolean which determines if the step was successfull,
 * if not a public variable "error" gives further informations why not.
 */
class H2o_Dev_FTP {
  /**
   * For instance Ftp needs credentials for the remote server
   * @param  {array} $ftp Array with ftp credentials
   * @return {boolean}      returns static true
   */
  public function __construct ($credentials){

        $this->host        = $credentials['host'];
        $this->user        = $credentials['user'];
        $this->pw          = $credentials['pw'];
        $this->targetdir   = $credentials['targetdir'];
        $this->ssl         = $credentials['ssl'];
        return true;
  }
  /**
   * Connect to remote ftp server appends connection to class and returns bool if successfull
   * @return {boolean}
   */
  public function connect(){
    // set up basic connection
    if ($this->ssl){
      if($this->connection = ftp_ssl_connect($this->host)){
        return true;
      }
    }
    if($this->connection = ftp_connect($this->host)){
        return true;
    }
    $this->error = 'Connection failed to host: '.  $this->host;
    return false;
  }
  /**
   * Login into remote ftp server with given credentials
   * @return {boolean}
   */
  public function login(){
    try {
      $this->connect();
      if(ftp_login($this->connection, $this->user, $this->pw)){
        return true;
      }
      return false;
    }
    catch (Exception $e){
      $this->error = $this->user . ' : ' . $this->pw . ' -> ' . substr($e->getMessage(),strpos($e->getMessage(),' ') + 1);
      return false;
    }
  }
  /**
   * Uploads filestream as file to a remote ftp server
   * @param  {resource} $filestream accepts fopen instance
   * @param  {string} $name       simple string to name the remote file
   * @return {boolean}            determines if upload was successfull
   */
  public function upload($filestream,$name){
      if (!$this->login()){
        return false;
      }

      ftp_pasv($this->connection, true);
      try {
        if(ftp_fput($this->connection, $this->targetdir . $name, $filestream, FTP_BINARY)){
          ftp_close($this->connection);
          return true;
        }
        return false;
      }
      catch (Exception $e){
        $this->error = substr($e->getMessage(),strpos($e->getMessage(),' ') + 1);
        ftp_close($this->connection);
        return false;
      }
  }
  /**
   * Downloads file and returns filestream
   * @param {string} $name name plus path e.g export/latest.xml
   * @return {resource} resource instance of the file
   */
  public function download($name){
    if (!$this->login()){
      return false;
    }
    ftp_pasv($this->connection, true);
    try {
        $fs = fopen('php://temp', "w+");
        ftp_fget($this->connection,$fs,$this->targetdir . $name, FTP_BINARY);
        ftp_close($this->connection);
        fseek($fs,0);
        return $fs;
    }
    catch (Exception $e){
      $this->error = substr($e->getMessage(),strpos($e->getMessage(),' ') + 1);
      echo $this->error;
      ftp_close($this->connection);
      return false;
    }
  }
  /**
   * Looksup availible files in remote dir and returns array
   * @param {string} $path path e.g export
   * @return {array} array with all dir/file names
   */
  public function listDir($path){
    if (!$this->login()){
      return false;
    }
    try {
      ftp_pasv($this->connection, true);
      $arr = ftp_nlist($this->connection,$path);
      if (!$arr){
        $this->error = "No files found";
        return false;
      }
      ftp_close($this->connection);
      return $arr;
    }
    catch (Exception $e){
      $this->error = substr($e->getMessage(),strpos($e->getMessage(),' ') + 1);
      ftp_close($this->connection);
      return false;
    }
  }
  /**
   * Deletes a given remote file
   *@param {string} File + full path
   *@return {boolean}
   */
  public function delete($file){
    if (!$this->login()){
      return false;
    }
    try {
      ftp_pasv($this->connection, true);
      ftp_delete($this->connection, $this->targetdir . $file);
      ftp_close($this->connection);
      return true;
    }
    catch (Exception $e){
      echo substr($e->getMessage(),strpos($e->getMessage(),' ') + 1);
      ftp_close($this->connection);
      return false;
    }
  }
}
