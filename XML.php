<?php
/**
 * XML Class
 *

 */
class H2o_Dev_XML {
  public function __construct (){
    return true;
  }
  /**
   * toArray()
   *@param {string} XML representation
   *@return {array}
   */
  public function toArray($xml,$main_heading = ''){
    $deXml = simplexml_load_string($xml);
    $deJson = json_encode($deXml);
    $xml_array = json_decode($deJson,TRUE);
    if (! empty($main_heading)) {
        $returned = $xml_array[$main_heading];
        return $returned;
    } else {
        return $xml_array;
    }
  }
}
