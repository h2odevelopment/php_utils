<?php
require_once("FTP.php");

class FtpTest extends PHPUnit_Framework_TestCase
{

    public function __construct (){
      $this->ftp = new H2o_Dev_Ftp([
        'host' => 'speedtest.tele2.net',
        'user' => 'anonymous',
        'pw'  => '',
        'targetdir' => 'upload/',
        'ssl' => false
        ]);
    }
    public function testConstructFtpClass()
    {
      $this->assertEquals('speedtest.tele2.net',$this->ftp->host);
    }
    public function testConnection(){
      $this->assertEquals(true,$this->ftp->connect());
    }
    public function testConnectionFail(){
      $this->ftp->host = 'localhost';
      $this->assertEquals(false,$this->ftp->connect());
      $this->assertEquals('Connection failed to host: localhost',$this->ftp->error);
    }
    public function testLogin(){
      $this->ftp->host = 'speedtest.tele2.net';
      $this->assertEquals(true,$this->ftp->login());
    }
    public function testLoginFail(){
      $this->ftp->host = 'speedtest.tele2.net';
      $this->ftp->user = 'testuser';
      $this->assertEquals(false,$this->ftp->login());
      $this->assertEquals('testuser :  -> This FTP server is anonymous only.',$this->ftp->error);

    }
    public function testUpload(){
      $fp = fopen('php://temp', 'r+');
      fwrite($fp, '<test></test>');
      rewind($fp);
      $this->ftp->user = 'anonymous';
      $this->assertEquals(true,$this->ftp->upload($fp,'test.xml'));
    }
    public function testUploadFail(){
      $fp = fopen('php://temp', 'r+');
      fwrite($fp, '<test></test>');
      rewind($fp);
      $this->ftp->targetdir = 'upload';
      $this->ftp->user = 'anonymous';
      $this->assertEquals(false,$this->ftp->upload($fp,'test'));
      $this->assertEquals('Could not create file.',$this->ftp->error);
    }
    public function testUploadFailLogin(){
      $fp = fopen('php://temp', 'r+');
      fwrite($fp, '<test></test>');
      rewind($fp);
      $this->ftp->host = 'ftp.h2om.de';
      $this->ftp->targetdir = 'import/';
      $this->ftp->user = 'Osile';
      $this->ftp->pw = 'Osile33';
      $this->assertEquals(false,$this->ftp->upload($fp,'test'));
      $this->assertEquals('Osile : Osile33 -> Login authentication failed',$this->ftp->error);
    }
    public function testDownload(){
      $this->ftp->user = 'anonymous';
      $this->ftp->targetdir = false;
      $this->assertEquals(1024,fstat($this->ftp->download('1KB.zip'))['size']);
    }
    public function testListDir(){
      $this->ftp->user = 'anonymous';
      $this->ftp->targetdir = false;
      $this->assertEquals('array',gettype($this->ftp->listDir('')));
    }
    public function testUploadReal(){
      $fp = fopen('php://temp', 'r+');
      fwrite($fp, '<?xml version="1.0" encoding="UTF-8"?>
<stockinfos xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="Stockinfo_Import.xsd">
   <stockinfo>
      <itemnumber>wbk005</itemnumber>
      <itemvariantcode></itemvariantcode>
      <stock>150</stock>
   </stockinfo>
</stockinfos>
');
      rewind($fp);
      $this->ftp->host = 'ftp.h2om.de';
      $this->ftp->user = 'test@h2om.de';
      $this->ftp->pw = 'Osilee33';
      $this->ftp->targetdir = false;
      $this->assertEquals(true,$this->ftp->upload($fp,'test.xml'));
    }
    public function testDownloadXML(){
      $this->ftp->host = 'ftp.h2om.de';
      $this->ftp->user = 'test@h2om.de';
      $this->ftp->pw = 'Osilee33';
      $this->ftp->targetdir = false;
      $testXML = '<?xml version="1.0" encoding="UTF-8"?>
<stockinfos xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="Stockinfo_Import.xsd">
   <stockinfo>
      <itemnumber>wbk005</itemnumber>
      <itemvariantcode></itemvariantcode>
      <stock>150</stock>
   </stockinfo>
</stockinfos>
';
      $this->assertEquals($testXML,stream_get_contents($this->ftp->download('test.xml')));
    }

    public function testDeleteFile(){
      $this->ftp->host = 'ftp.h2om.de';
      $this->ftp->user = 'test@h2om.de';
      $this->ftp->pw = 'Osilee33';
      $this->ftp->targetdir = false;
      $this->assertEquals(true,$this->ftp->delete('test.xml'));
    }
    
}
