<?php
require_once("XML.php");


class XMLTest extends PHPUnit_Framework_TestCase
{
    public function __construct (){
      $this->xml = new H2o_Dev_XML();
    }
    public function testRead(){
      $this->assertArrayHasKey('b',$this->xml->toArray('<a><b></b></a>'));
    }
}
